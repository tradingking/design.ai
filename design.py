# @shifeng 20200606

import pandas as pd
import jiagu
import time
from datetime import datetime
from os import path
import random
import jellyfish

# env parameters
road_file = './dict/road.txt'
emotion_file = './dict/emotion.csv'
sense_file = './dict/sensitive_words.txt'
id_file = './data/id_file.txt'
input_file = 'j:/mary/laohuang.csv'
output_file = 'j:/mary/output_data.csv'

# model parameters
max_lines = 100000
sleep = 5
max_letters = 50


def add_jieci():
    pass

def get_global_id():
    with open(id_file) as f:
        global_id = f.read()
    return int(global_id)


def set_global_id(global_id):
    with open(id_file, 'w') as f:
        f.write(str(global_id))


def check_outfile():
    # if file exist , if there's content, then add no header
    # otherwise create new file and add header
    if path.getsize(output_file) == 0 or path.exists(output_file) is False:
        with open(output_file, 'a') as f:
            f.writelines(
                'id,time,user,location_id,location,emotion_word,emotion,keyword,zimu\n'.
                format())

def read_file():
    columns = ['user', 'raw']
    df = pd.DataFrame(columns=columns)
    with open(input_file, encoding='utf-8') as fp:
        cnt = 0
        for _, line in enumerate(fp):
            try:
                user = line.strip().split(',')[0]
                raw = '.'.join(line.split(',')[1:])
                raw = raw.strip('\n')
                df.loc[cnt] = [user, raw]
            except Exception as e:
                print('incorrect input:{}'.format(e))
                df.loc[cnt] = [
                    '神秘人',
                    '{}.我的想法很独特。i am so special'.format(_get_random_road())
                ]
            cnt += 1
    return df


def red_file_pandas():
    df_raw = pd.read_csv(input_file)
    return df_raw


def clean_content(content):
    for s in sense_list:
        content = content.replace(s, '**')
    content = content.replace(',', '.')
    # content = content.replace(' ','')
    return content


def _default_line():
    line = '神秘人,{}.我的想法很独特。i am so special'.format(_get_random_road())
    return line


def _get_random_road():
    r = random.randint(0, len(road_list))
    return road_list[r]


def _get_similar_road(text):
    scores = []
    for road in road_list:
        scores.append(jellyfish.jaro_distance(road, text))
    data = {'road': road_list, 'score': scores}
    df = pd.DataFrame.from_dict(data)
    df = df.sort_values(by=['score'], ascending=False)
    # print(df[:3])
    return df.iloc[0][0]


def get_location(text,words):
    loc_list = []
    for w in words:
        if w in road_list and w not in loc_list:
            loc_list.append(w)

    # check if loc is empty
    # is so assign random
    if len(loc_list) == 0:
        # loc_list.append(_get_random_road())
        loc_list.append(_get_similar_road(text))

    # find the loc_id
    loc_id = []
    for l in loc_list:
        loc_id.append(str(road_list.index(l)))
    return loc_list, loc_id


def _get_similar_emotion(text):
    scores = []
    for e in emotion_similar:
        scores.append(jellyfish.jaro_distance(e, text))
    data = {'emotion': emotion_similar, 'score': scores}
    df = pd.DataFrame.from_dict(data)
    df = df.sort_values(by=['score'], ascending=False)
    # print(df[:3])
    return df.iloc[0][0]

def get_emotion(text,words):
    for w in words:
        for i in range(len(emotion_similar)):
            if w == emotion_similar[i]:
                return w,emotion_score[i]

    emotion_word = _get_similar_emotion(text)
    escore = emotion_score[emotion_similar.index(emotion_word)]
    return emotion_word, escore


def get_sentiment (text):
    pn, score = jiagu.sentiment(text)
    if pn == 'negative':
        o_sentiment = int(-20 * score)
        if o_sentiment <-16:
            o_sentiment = int(o_sentiment/2)
        else:
            o_sentiment = min(o_sentiment+15,9)
    else:
        o_sentiment = int(10 * score)
    return o_sentiment



def analysis(text):
    """分析一句话的几个维度
    :text: 一句话
    """
    o_id = global_id
    o_time = datetime.now().strftime('%Y%m%d-%H:%M:%S.%f')[:-3]
    user = one['user']
    words = jiagu.seg(text)  # 分词
    loc_list, loc_id = get_location(text,words)
    o_loc = '_'.join(loc_list)
    o_loc_id = '_'.join(loc_id)
    keyword_list = jiagu.keywords(text, 3)
    o_keyword = '_'.join(keyword_list)
    emotion_word, o_emotion = get_emotion(text,words)
    line = '{},{},{},{},{},{},{},{},{}'.format(o_id, o_time, user, o_loc_id,
                                            o_loc, emotion_word, o_emotion, o_keyword,
                                            text[:max_letters])
    return line


# 1. load knowledge
df_road = pd.read_csv(road_file, header=None)
road_list = df_road[0].tolist()
df_sense = pd.read_csv(sense_file, header=None)
sense_list = df_sense[0].tolist()
df_emotion = pd.read_csv(emotion_file)
emotion_similar = df_emotion['similar'].tolist()
emotion_score = df_emotion['score'].tolist()

jiagu.load_userdict(road_list)
# 2. read in the content
cur_pos = 0
global_id = get_global_id()
check_outfile()
# find the new input
while cur_pos < max_lines:
    df_raw = read_file()
    if cur_pos >= len(df_raw):
        print('[info]: No new input. sleep')
        time.sleep(10)
        continue
    one = df_raw.loc[cur_pos]
    content_raw = one['raw']
    # 3. emotion analysis
    text = clean_content(content_raw)
    line = analysis(text)
    if line == '':
        continue
    print('[#{}]:{}'.format(cur_pos + 1, line))
    # 4. output
    with open(output_file, 'a', encoding='utf-8') as f:
        f.writelines(line + '\n')
    global_id += 1
    set_global_id(global_id)
    cur_pos += 1
    time.sleep(sleep)
