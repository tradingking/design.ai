<html>
    <head><title>{{title}}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
    </head>
    <body>
         <h1>{{title}}</h1>

         <p>Ideas you like:</p>
         % if likes:
         <ul  >
             % for like in likes:
              <li>{{like}}</li>
             % end
         </ul>
         % end
          <form method='POST' action='/likes'  accept-charset="UTF-8" οnsubmit="document.charset='UTF-8'">
              <legend>your design ideas? </legend>
              <ul>
                <li><textarea name="likes" rows="20" cols="80""></textarea></li>
              </ul>
              <input type='submit' value='Submit'  align="center">
          </form>

    </body>
</html>
