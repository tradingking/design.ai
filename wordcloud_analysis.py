import pandas as pd
import matplotlib.pyplot as plt
import jieba
import jieba.analyse
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator


def get_random_sentence():
    return

# read in the content
# df = pd.read_csv('input.txt')
df = pd.read_csv('group_input.txt')

sentence = df.values.tolist()[0][0]

# analize the content
words = " ".join(jieba.cut(sentence))

# configure the drawing of all words
img = plt.imread("china_map.jpg")
wc = WordCloud(background_color="white",      
     mask=img,#设置背景图片     
     max_font_size=120, #字体最大值    
     random_state=42,  #颜色随机性 
     font_path="simhei.ttf",
     max_words=2000, # 设置最大现实的字数    
     stopwords=STOPWORDS,# 设置停用词 
     )
wc.generate(words)

# draw words picture
image_colors = ImageColorGenerator(img)
plt.figure(figsize=(14,12))
plt.imshow(wc)
plt.title('AI Design',fontsize=40)
plt.axis("off")
plt.show()