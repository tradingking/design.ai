# by shifeng 20200606
# https://towardsdatascience.com/animations-with-matplotlib-d96375c5442c

import pandas as pd
import matplotlib.pyplot as plt
import jieba
import jieba.analyse
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import time
import matplotlib.animation as animation

def get_random_sentence():
    return


# find the corresponding content to process
def process_sentence(i):
    sentence =''
    stg = strategy['type']
    if stg == 'combine':
        if i< len(df):
            for ind in range(i+1):
                sentence += df['content'][ind]
        else:
            for ind in range(len(df)):
                sentence += df['content'][ind]
    else:
        sentence = df['content'][i%len(df)]
    print(i,len(sentence),sentence[:20])
    return sentence


def process_image(i):
    sentence = process_sentence(i)
    # analyze the content
    words = " ".join(jieba.cut(sentence))
    wc.generate(words)
    # draw words picture
    ax1.clear()
    plt.imshow(wc)
    plt.title(df['name'][i%len(df)],fontsize=40)
    plt.axis("off")


# configure the drawing of all words
img = plt.imread("./image/china_map.jpg")
image_colors = ImageColorGenerator(img)
wc = WordCloud(background_color="white",      
     mask=img,#设置背景图片     
     max_font_size=120, #字体最大值    
     random_state=42,  #颜色随机性 
     font_path="simhei.ttf",
     max_words=2000, # 设置最大现实的字数    
     stopwords=STOPWORDS,# 设置停用词
     # color_func=image_colors
     )


# read in the content
# df = pd.read_csv('input.txt')
df = pd.read_csv('./data/group_input.txt')
df = pd.read_csv('J:/mary/group_input.txt')
strategy = {'type': 'single'}
cur_fig = plt.figure(figsize=(14,12))
ax1 = cur_fig.add_subplot(1,1,1)
ani = animation.FuncAnimation(cur_fig, process_image,interval=5000) 
plt.show()